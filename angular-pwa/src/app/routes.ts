import { CocotteComponent } from './templates/cocotte/cocotte.component';

import { Routes } from '@angular/router';
import { HomeComponent } from './templates/home/home.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    data: {animation: 'HomePage'}
  },
  {
    path: 'cocotte',
    component: CocotteComponent,
    data: {animation: 'CocottePage'}
  },
];
