import { Component, Input, ViewEncapsulation } from '@angular/core';
import { IconAbstractComponent } from '../icon-abstract/icon-abstract.component';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IconComponent extends IconAbstractComponent {
  @Input() name = '';
  @Input('aria-label') ariaLabel: string | undefined;

  constructor() {
    super();
  }
}
