import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconComponent } from './icons/icon/icon.component';
import { IconAbstractComponent } from './icons/icon-abstract/icon-abstract.component';
import { IconHeartComponent } from './icons/icon-heart/icon-heart.component';
import { IconBackComponent } from './icons/icon-back/icon-back.component';

@NgModule({
  declarations: [IconComponent, IconAbstractComponent, IconHeartComponent, IconBackComponent],
  imports: [CommonModule],
  exports: [IconComponent, IconAbstractComponent, IconHeartComponent],
})
export class SharedModule {}
