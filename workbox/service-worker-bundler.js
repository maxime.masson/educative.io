/**
 * npm install workbox-precaching
 * Note : if we work with Webpack, we can use the workbox-webpack-plugin to abstracts much of the complexity.
 */
import {precacheAndRoute} from 'workbox-precaching';
// Other eventual imports here

precacheAndRoute([
  {url: '/index.html', revision: '13245'},
  {url: '/styles/base.css', revision: null},
  {url: '/scripts/starter.js', revision: null},
]);
